package com.izliang.timeutils.utils;

import com.izliang.timeutils.model.BeginAndEnd;
import com.izliang.timeutils.model.ConstantModel;
import com.izliang.timeutils.model.SeriesResult;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
* 时间段是否连续判断
* */
public class SeriesUtils {

    /**
     * 将格式化的时间字符换列表转换为自己的模型
     * @param data
     * @return
     * @throws Exception
     */
    public static List<BeginAndEnd>formatNormDateNoBreakTDate(List<String> data)throws Exception{

        List<BeginAndEnd> beginAndEndList = new ArrayList<>();

        try {
            //现在拿到了
            if(ObjectUtils.isEmpty(data)){
                throw new Exception("数据为空");
            }

            for (String str:data) {

                if(StringUtils.isEmpty(str)){
                    throw new Exception(str+"处，数据为空");
                }

                String[] strArr = str.split("-");
                if(strArr.length != 2){
                    throw new Exception("'"+str+"'不符合数据格式，缺少'-',或者数据有问题");
                }

                if(StringUtils.isEmpty(strArr[0])){
                    throw new Exception(str+"开始时间为空");
                }
                if(StringUtils.isEmpty(strArr[1])){
                    throw new Exception(str+"结束时间为空");
                }


                //现在处理数据
                try {
                    Date dateBegin = ConstantModel.NORM_DATE_NO_BREAK_FORMAT.parse(strArr[0]);
                    try {
                        Date dateEnd  = ConstantModel.NORM_DATE_NO_BREAK_FORMAT.parse(strArr[1]);
                        if(dateBegin.getTime() > dateEnd.getTime()){

                            //开始时间大于结束时间
                            throw new Exception(str+"开始时间大于结束时间");
                        }


                        BeginAndEnd beginAndEnd = new BeginAndEnd();

                        beginAndEnd.setbDate(dateBegin);
                        beginAndEnd.seteDate(dateEnd);

                        beginAndEndList.add(beginAndEnd);

                    }catch (Exception e){
                        throw new Exception(strArr[1]+"转换Date类型失败，当前格式"+ConstantModel.NORM_DATE_NO_BREAK_FORMAT.toString());
                    }
                }catch (Exception e){
                    throw new Exception(strArr[0]+"转换Date类型失败，当前格式"+ConstantModel.NORM_DATE_NO_BREAK_FORMAT.toString());
                }
            }
        }catch (Exception e){
            throw new  Exception(e.getMessage());
        }
        return beginAndEndList;
    }

    /**
     * 获取列表中最大的时间和最小的时间
     * @param beginAndEndList
     * @return
     */
    public static BeginAndEnd getMinBeginAndMaxEnd(List<BeginAndEnd> beginAndEndList){

        long minBegin = Long.MAX_VALUE;
        long maxEnd   = Long.MIN_VALUE;

        for (BeginAndEnd beginAndEnd: beginAndEndList) {
            minBegin = beginAndEnd.getbDate().getTime() < minBegin ? beginAndEnd.getbDate().getTime() : minBegin;
            maxEnd   = beginAndEnd.geteDate().getTime() > maxEnd   ? beginAndEnd.geteDate().getTime() : maxEnd;
        }
        BeginAndEnd beginAndEnd = new BeginAndEnd();
        beginAndEnd.setbDate(new Date(minBegin));
        beginAndEnd.seteDate(new Date(maxEnd));

        return beginAndEnd;
    }

    /**
     * 获取两者之间的天数
     * @param beginAndEnd
     * @return
     */
    public static int getDaysByBeginAndEnd(BeginAndEnd beginAndEnd){

        return (int)((beginAndEnd.geteDate().getTime() - beginAndEnd.getbDate().getTime()) / ConstantModel.DAY_MS);
    }

    /**
     * 获取区间连续状态
     * @param res
     * @return
     */
    public static boolean getIsSeriesStatus(boolean[] res){

        for (boolean r:res) {
            if(!r){
                return false;
            }
        }
        return true;
    }

    public static List<BeginAndEnd> getNoSeriesArea(BeginAndEnd beginAndEnd,boolean[] res){

        List<BeginAndEnd> beginAndEndList = new ArrayList<>();

        boolean isOpen = false;

        BeginAndEnd beginAndEnd1 = new BeginAndEnd();
        int index = 0;
        for (boolean r:res) {
            if(!r && !isOpen){
                //现在记录开始
                beginAndEnd1.setbDate(new Date(beginAndEnd.getbDate().getTime() + index * ConstantModel.DAY_MS));
                isOpen = true;
            }

            if(!r && isOpen){
                //现在更新结束时间
                beginAndEnd1.seteDate(new Date(beginAndEnd.getbDate().getTime() + index * ConstantModel.DAY_MS));
            }

            if(r){
                if(beginAndEnd1.getbDate() != null && beginAndEnd1.getbDate().getTime() > 0){
                    beginAndEndList.add(beginAndEnd1);
                    beginAndEnd1 = new BeginAndEnd();
                }
                isOpen = false;
            }

            index++;
        }

        return beginAndEndList;
    }


    public static SeriesResult isSeriesDayB(BeginAndEnd beginAndEnd,boolean[] res,List<BeginAndEnd> beginAndEndList){

        long bb = beginAndEnd.getbDate().getTime();
        //long ee = beginAndEnd.geteDate().getTime();

        //现在开始填充boolean数组
        for (BeginAndEnd beginAndEnd1:beginAndEndList) {

            //现在获取要填充的索引
            long b = beginAndEnd1.getbDate().getTime();
            long e = beginAndEnd1.geteDate().getTime();

            int bIndex = (int)((b-bb)/ConstantModel.DAY_MS);
            int eIndex = (int)((e-bb)/ConstantModel.DAY_MS);
            paddingArr(res,bIndex,eIndex);

        }

        //现在根据索引返回时间
        SeriesResult seriesResult = new SeriesResult();
        boolean isSeries = getIsSeriesStatus(res);
        seriesResult.setResultStatus(isSeries);
        seriesResult.setBeginAndEnd(beginAndEnd);
        seriesResult.setNoSeriesArea(!isSeries?getNoSeriesArea(beginAndEnd,res) : new ArrayList<>());

        return seriesResult;
    }

    /**
     * 填充数组
     * @param res
     * @param bIndex
     * @param eIndex
     * @return
     */
    public static boolean[] paddingArr(boolean[] res,int bIndex,int eIndex){
        for (int i = 0; i < res.length; i++) {
            if(i >= bIndex && i <= eIndex){
                res[i] = true;
            }
        }
       // System.out.println(Arrays.toString(res));
        return res;
    }

    /**
     * 根据两个date列表获取连续结果
     * @param data [20200312-20200413,20200316-20200411]
     * @return
     */
    public static SeriesResult isSeriesDay(List<String> data) {
        try{
            List<BeginAndEnd> beginAndEndList = formatNormDateNoBreakTDate(data);

            //获取最早的开始和最晚的结束
            BeginAndEnd beginAndEnd = getMinBeginAndMaxEnd(beginAndEndList);

            //现在获取到了最长的时间跨度，的下标，那么现在就可以
            int arrLength = getDaysByBeginAndEnd(beginAndEnd);

            //现在。没有找到异常
            boolean[] res = new boolean[arrLength];

            //现在开始填充这个数组
            return isSeriesDayB(beginAndEnd,res,beginAndEndList);

        }catch (Exception e){
            SeriesResult seriesResult = new SeriesResult();

            seriesResult.setResultStatus(false);
            seriesResult.setResultMsg(e.getMessage());
            return seriesResult;
        }
    }

}
