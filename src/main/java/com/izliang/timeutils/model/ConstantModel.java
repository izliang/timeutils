package com.izliang.timeutils.model;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * 常量类
 */
public class ConstantModel {

    /** 毫秒 */
    public final static long MS = 1;
    /** 每秒钟的毫秒数 */
    public final static long SECOND_MS = MS * 1000;
    /** 每分钟的毫秒数 */
    public final static long MINUTE_MS = SECOND_MS * 60;
    /** 每小时的毫秒数 */
    public final static long HOUR_MS = MINUTE_MS * 60;
    /** 每天的毫秒数 */
    public final static long DAY_MS = HOUR_MS * 24;

    /** 标准日期（不含时间）格式化器 */
    public final static SimpleDateFormat NORM_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    /** 标准日期（不含时间）格式化器-没有分割符 */
    public final static SimpleDateFormat NORM_DATE_NO_BREAK_FORMAT = new SimpleDateFormat("yyyyMMdd");
    /** 标准日期时间格式化器 */
    public final static SimpleDateFormat NORM_DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /** HTTP日期时间格式化器 */
    public final static SimpleDateFormat HTTP_DATETIME_FORMAT = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);

}
