package com.izliang.timeutils.model;

import java.util.Date;

public class BeginAndEnd {

    private Date bDate;

    private Date eDate;

    public Date getbDate() {
        return bDate;
    }

    public void setbDate(Date bDate) {
        this.bDate = bDate;
    }

    public Date geteDate() {
        return eDate;
    }

    public void seteDate(Date eDate) {
        this.eDate = eDate;
    }

    @Override
    public String toString() {
        return "BeginAndEnd{" +
                "bDate=" + bDate +
                ", eDate=" + eDate +
                '}';
    }
}
