package com.izliang.timeutils.model;

import java.util.List;

public class SeriesResult {

    //返回状态
    private boolean resultStatus;

    private String resultMsg;

    //是否连续
    private boolean isSeries;

    //连续区间
    private BeginAndEnd beginAndEnd;

    //不连续的区间
    private List<BeginAndEnd> noSeriesArea;

    public boolean isSeries() {
        return isSeries;
    }

    public void setSeries(boolean series) {
        isSeries = series;
    }

    public BeginAndEnd getBeginAndEnd() {
        return beginAndEnd;
    }

    public void setBeginAndEnd(BeginAndEnd beginAndEnd) {
        this.beginAndEnd = beginAndEnd;
    }

    public List<BeginAndEnd> getNoSeriesArea() {
        return noSeriesArea;
    }

    public void setNoSeriesArea(List<BeginAndEnd> noSeriesArea) {
        this.noSeriesArea = noSeriesArea;
    }

    public boolean isResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(boolean resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    @Override
    public String toString() {
        return "SeriesResult{" +
                "resultStatus=" + resultStatus +
                ", resultMsg='" + resultMsg + '\'' +
                ", isSeries=" + isSeries +
                ", beginAndEnd=" + (null == beginAndEnd ? "": beginAndEnd.toString()) +
                ", noSeriesArea=" + ( null == noSeriesArea ? "" : noSeriesArea.toString()) +
                '}';
    }
}
