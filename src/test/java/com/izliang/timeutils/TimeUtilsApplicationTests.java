package com.izliang.timeutils;

import com.izliang.timeutils.model.BeginAndEnd;
import com.izliang.timeutils.model.SeriesResult;
import com.izliang.timeutils.utils.SeriesUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class TimeUtilsApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void name() {
        List<String> data = new ArrayList<>();
        String s1 = "20100315-20120621";
        data.add(s1);
        String s2 = "20120623-20210612";
        data.add(s2);
        SeriesResult seriesResult =  SeriesUtils.isSeriesDay(data);

        System.out.println(seriesResult.toString());
    }

    @Test
    void name1() {
        BeginAndEnd beginAndEnd1 = new BeginAndEnd();

        System.out.println(beginAndEnd1);

        System.out.println(beginAndEnd1.getbDate());
    }
}
