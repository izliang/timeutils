# 时间区间连续断档工具

#### 介绍
java 时间断档工具，用来检测时间段是否连续，多个时间段是否存在不连续的区间等

#### 软件架构
springboot 2.2.5.RELEASE



#### 使用说明

    @Test
    void name() {
        List<String> data = new ArrayList<>();
        String s1 = "20100315-20120621";
        data.add(s1);
        String s2 = "20120623-20210612";
        data.add(s2);
        SeriesResult seriesResult =  SeriesUtils.isSeriesDay(data);

        System.out.println(seriesResult.toString());
    }

#### 返回结果

    //返回状态
    private boolean resultStatus;
    //返回信息
    private String resultMsg;
    //是否连续
    private boolean isSeries;
    //连续区间
    private BeginAndEnd beginAndEnd;

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


